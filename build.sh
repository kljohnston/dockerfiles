#!/bin/bash
# Builds a given docker image

##### constants
#######################################################
INVALID_INPUT=1
NAMESPACE="kevinjohnston"
BUILD_NUM_FILE=.build-num



##### variables
#######################################################
IMAGE_NAME="$1"
TESTING=${2:-1}
NO_CACHE=${3:-0}



##### functions
#######################################################

print_help(){
  echo "TODO write help"
}

verify_inputs(){
  if [ -z "$IMAGE_NAME" ]; then
    print_help
    exit $INVALID_INPUT
  fi
}

build_num(){
  echo $(cat $BUILD_NUM_FILE | grep -Po '(?<=)[^;]+')
}

testing_tag_info(){
  echo -n "$NAMESPACE/$IMAGE_NAME:test"
}

release_tag_info(){
  echo -n "$NAMESPACE/$IMAGE_NAME:v$(cat ./.build-num)"
}

increment_build_num(){
  BUILD_NUM=$(build_num)
  NEW_BUILD_NUM=$(echo "${BUILD_NUM%.*}.$((${BUILD_NUM##*.}+1))")
  echo "$NEW_BUILD_NUM" > $BUILD_NUM_FILE
}

is_logged_in(){
  USERNAME=${1:-kevinjohnston}
  docker info 2>/dev/null | grep -e "^Username: $USERNAME\$"
  return $?
}

msg(){
  echo "##### $*"
  echo "#######################################################"
  echo 
}

build_docker_image(){
  cd ./$IMAGE_NAME
  if [ "$TESTING" -gt 0 ]; then
    msg Building test image $(testing_tag_info)
    if [ "$NO_CACHE" -gt 0 ]; then
      docker build . --no-cache -t $(testing_tag_info)
    else
      docker build . -t $(testing_tag_info)
    fi
  elif [ "$TESTING" -eq 0 ]; then
    msg Building release image $(release_tag_info)
    increment_build_num
    docker build . --no-cache -t $(release_tag_info)
    docker build . -t $NAMESPACE/$IMAGE_NAME:latest
    if [ $(is_logged_in) -gt 0 ]; then
      docker login
    fi
    docker push $(release_tag_info)
    docker push $NAMESPACE/$IMAGE_NAME:latest
  fi
}



##### logic
#######################################################
verify_inputs
build_docker_image
